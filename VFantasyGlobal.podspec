Pod::Spec.new do |spec|

spec.name         = "VFantasyGlobal"
spec.version      = "1.11.1"
spec.summary      = "A short description of VFantasyGlobal."
spec.description  = "A short description of VFantasyGlobal."

spec.platform     = :ios, "11.0"

spec.homepage     = "http://EXAMPLE/VFantasyGlobal"
spec.license      = "MIT"
# spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
spec.author             = { "Quang Tran" => "tranquangbk56@gmail.com" }

spec.source       = { :path => "." }
# spec.exclude_files = "Classes/Exclude"
# spec.source       = { :git => "https://gitlab.com/Soict/vfantasyglobal.git", :tag => "#{spec.version}" }

spec.dependency "SwiftDate"
spec.dependency "MZFormSheetPresentationController"
spec.dependency "SDWebImage"
spec.dependency "Alamofire"
spec.dependency "MBProgressHUD"
spec.dependency "PureLayout"
spec.dependency "HMSegmentedControl"
spec.dependency "SMPageControl"
spec.dependency "SkeletonView"
spec.dependency "IQKeyboardManagerSwift"
spec.dependency "SVPullToRefresh"
spec.dependency "SwiftyJSON"

#spec.subspec 'XCFrameworkPod' do |xcframework|
  #xcframework.vendored_frameworks = 'VFantasyGlobal.xcframework'
#end

spec.subspec 'Global' do |global|
global.header_dir   =  'VFantasyGlobal'
global.subspec 'XCFrameworkPod' do |xcframework|
  xcframework.vendored_frameworks = 'VFantasyGlobal.xcframework'
end
end

spec.subspec 'Jackpot' do |jackpot|
jackpot.header_dir   =  'VFantasyGlobal'
jackpot.subspec 'XCFrameworkPod' do |xcframework|
  xcframework.vendored_frameworks = 'VFantasyJackpot.xcframework'
end
end

end
